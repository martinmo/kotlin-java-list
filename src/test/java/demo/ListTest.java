package demo;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class ListTest {

    @Test
    public void demo() {
        Collection<Integer> collection = Arrays.asList(1, 2, 3);

        // does not create a new collection, but uses the existing one
        L<Integer> l = L.of(collection);
        assertThat(l).isInstanceOf(Collection.class);
        assertThat(l).containsExactly(1, 2, 3);

        L<String> mapped = l.map(String::valueOf);
        assertThat(mapped).containsExactly("1", "2", "3");

        L<Integer> even = l.filter(i -> i % 2 == 0);
        assertThat(even).containsExactly(2);

        Optional<Integer> three = l.find(i -> i == 3);
        assertThat(three).contains(3);

        Map<Boolean, L<Integer>> evenAndOdd = l.groupBy(i -> i % 2 == 0);
        assertThat(evenAndOdd.get(true)).containsExactly(2);
        assertThat(evenAndOdd.get(false)).containsExactly(1, 3);

        Integer sum = l.fold(0, Integer::sum);
        assertThat(sum).isEqualTo(6);

        L<String> strings = L.of("ab", "cd").flatMap(s -> L.of(s.split("")));
        assertThat(strings).containsExactly("a", "b", "c", "d");
    }

    @Test
    public void canBuildFromCollection() {
        Collection<String> collection = Collections.singleton("L");

        L<String> l = L.of(collection);

        assertThat(l).isInstanceOf(Collection.class);
        assertThat(l).containsExactly("L");
    }

    @Test
    public void canMap() {
        L<Integer> list = L.of(1, 2);

        L<Integer> mapped = list.map(e -> e + 1);

        assertThat(mapped).containsExactly(2, 3);
        assertThat(mapped).isInstanceOf(Collection.class);
    }

    @Test
    public void canMapCompose() {
        L<Integer> list = L.of(1, 2);
        Function<Integer, Integer> addOne = i -> i + 1;

        L<String> mapped = list.map(addOne.andThen(String::valueOf));

        assertThat(mapped).containsExactly("2", "3");
    }

    @Test
    public void canFilter() {
        L<Integer> list = L.of(1, 2, 3);

        L<Integer> uneven = list.filter(e -> e % 2 != 0);

        assertThat(uneven).containsExactly(1, 3);
        assertThat(uneven).isInstanceOf(Collection.class);
    }

    @Test
    public void canFind() {
        L<Integer> list = L.of(1, 2, 3);

        Optional<Integer> two = list.find(e -> e == 2);

        assertThat(two).hasValue(2);
    }

    @Test
    public void canFindNothing() {
        L<Integer> list = L.of(1, 2, 3);

        Optional<Integer> ten = list.find(e -> e == 10);

        assertThat(ten).isEmpty();
    }

    @Test
    public void canReduce() {
        L<Integer> list = L.of(1, 2, 3);

        Integer sum = list.fold(0, (acc, e) -> acc + e);

        assertThat(sum).isEqualTo(6);
    }

    @Test
    public void canFlatMap() {
        L<String> list = L.of("ab", "cd");

        L<String> chars = list.flatMap(str -> Arrays.asList(str.split("")));

        assertThat(chars).containsExactly("a", "b", "c", "d");
        assertThat(chars).isInstanceOf(Collection.class);
    }

    @Test
    public void canGroupBy() {
        L<String> list = L.of("a", "bc", "de");

        Map<Integer, L<String>> grouped = list.groupBy(String::length);

        assertThat(grouped).containsOnlyKeys(1, 2);
        assertThat(grouped.get(1)).containsExactly("a");
        assertThat(grouped.get(2)).containsExactly("bc", "de");
    }

    @Test
    public void canGroupByWithJoiner() {
        L<String> list = L.of("a", "bc", "de");

        Map<Integer, String> grouped = list.groupBy(
                String::length,
                entry -> entry.getValue().fold("", String::concat));

        assertThat(grouped).containsOnlyKeys(1, 2);
        assertThat(grouped.get(1)).isEqualTo("a");
        assertThat(grouped.get(2)).isEqualTo("bcde");
    }

    @Test
    public void canChain() {
        L<String> list = L.of("1", "2");

        Optional<Integer> found = list.map(Integer::parseInt).find(i -> i == 2);

        assertThat(found).hasValue(2);
    }

    @Test
    public void createsImmutableCollection() {
        L<String> list = L.of();
        assertThatThrownBy(() -> list.add("1")).isOfAnyClassIn(UnsupportedOperationException.class);

        L<String> fromCollection = L.of(new ArrayList<String>());
        assertThatThrownBy(() -> fromCollection.add("1")).isOfAnyClassIn(UnsupportedOperationException.class);
    }

}
