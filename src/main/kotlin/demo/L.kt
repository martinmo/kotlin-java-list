package demo

import java.util.Optional
import java.util.function.BiFunction
import java.util.function.Function
import java.util.function.Predicate

class L<E>(private val list: Collection<E>) : Collection<E> by list {

    fun <T> map(f: Function<E, T>): L<T> = list.map(f::apply).toL()

    fun filter(f: Predicate<E>): L<E> = list.filter(f::test).toL()

    fun find(f: Predicate<E>): Optional<E> = list.find(f::test).toOptional()

    fun <T> fold(initial: T, f: BiFunction<T, E, T>): T = list.fold(initial, f::apply)

    fun <T> flatMap(f: Function<E, Collection<T>>): L<T> = list.flatMap(f::apply).toL()

    fun <T> groupBy(f: Function<E, T>): Map<T, L<E>> = list.groupBy(f::apply).mapValues { L.of(it.value) }

    fun <T, N> groupBy(f: Function<E, T>, entryMapper: Function<Map.Entry<T, L<E>>, N>): Map<T, N> =
            this.groupBy(f).mapValues(entryMapper::apply)

    private companion object {
        @JvmStatic
        fun <E> of(list: Collection<E>): L<E> = L(list.toList())

        @JvmStatic
        fun <E> of(vararg elements: E): L<E> = L(elements.asList())

        private fun <E> List<E>.toL(): L<E> = L(this)

        private fun <T> T?.toOptional(): Optional<T> = Optional.ofNullable(this)
    }
}
