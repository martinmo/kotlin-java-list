
Extend Java Collections with with functional operators
======================================================

Small demo that shows how you can extended Java's Collection API with functional operators.
The actual [implementation](https://bitbucket.org/martinmo/kotlin-java-list/src/master/src/main/kotlin/demo/L.kt) 
is few lines of Kotlin using Kotlin's [delegation](https://kotlinlang.org/docs/reference/delegation.html)
feature.

```java
public class ListTest {

          @Test
          public void demo() {
              Collection<Integer> collection = Arrays.asList(1, 2, 3);
      
              // does not create a new collection, but uses the existing one
              L<Integer> l = L.of(collection);
              assertThat(l).isInstanceOf(Collection.class);
              assertThat(l).containsExactly(1, 2, 3);
      
              L<String> mapped = l.map(String::valueOf);
              assertThat(mapped).containsExactly("1", "2", "3");
      
              L<Integer> even = l.filter(i -> i % 2 == 0);
              assertThat(even).containsExactly(2);
      
              Optional<Integer> three = l.find(i -> i == 3);
              assertThat(three).contains(3);
      
              Map<Boolean, L<Integer>> evenAndOdd = l.groupBy(i -> i % 2 == 0);
              assertThat(evenAndOdd.get(true)).containsExactly(2);
              assertThat(evenAndOdd.get(false)).containsExactly(1, 3);
      
              Integer sum = l.fold(0, Integer::sum);
              assertThat(sum).isEqualTo(6);
      
              L<String> strings = L.of("ab", "cd").flatMap(s -> Arrays.asList(s.split("")));
              assertThat(strings).containsExactly("a", "b", "c", "d");
          }
}
```

More examples [src/master/src/test/java/demo/ListTest.java](https://bitbucket.org/martinmo/kotlin-java-list/src/master/src/test/java/demo/ListTest.java)

[JavaDoc](https://htmlpreview.github.io/?https://bitbucket.org/martinmo/kotlin-java-list/raw/master/javadoc/index.html)

Implementation [src/main/kotlin/demo/L.kt](https://bitbucket.org/martinmo/kotlin-java-list/src/master/src/main/kotlin/demo/L.kt)

Notes
-----
* By using delegation, we can reuse an existing instance of a given Collection. 
It is similar to Guava's [ForwardingList](https://google.github.io/guava/releases/19.0/api/docs/com/google/common/collect/ForwardingList.html).
* By using delegation, we can avoid inheritance.
* The resulting collection is read only, throwing UnsupportedOperationException if you try to mutate it.
 Sadly Java does not have a dedicated interface for immutable collections. 
* All operations are eager (not lazy like in Java Streams).
* Map and filter create a new collections -> try to avoid chaining them. 
* It is surprisingly easy to create an idiomatic Java-API from Kotlin. [JavaDoc](https://htmlpreview.github.io/?https://bitbucket.org/martinmo/kotlin-java-list/raw/master/javadoc/index.html).
* The implementation is not exactly idiomatic Kotlin code, because it uses Function, BiFunction and Predicate. 
In Kotlin there is first class function support and we don't need them. They just make the Java-API a bit nicer.
Same is true for Optional.
* If you use Java collections from within Kotlin there is no need for stuff like this, as the Kotlin std-lib already extends the Java API.
